if (FormsSubject.find().count() === 0) {
    FormsSubject.insert({
        name: 'lecture'
    });

    FormsSubject.insert({
        name: 'pract'
    });

    FormsSubject.insert({
        name: 'halava'
    });
}

if (Classroom.find().count() === 0) {
    Classroom.insert({
        name: 'K2-305',
        capasity:35
    });
    Classroom.insert({
        name: 'K5-345',
        capasity:32
    });
    Classroom.insert({
        name: 'K6-335',
        capasity:31
    });
    Classroom.insert({
        name: 'K1-123',
        capasity:42
    });
    Classroom.insert({
        name: 'K2-34',
        capasity:33
    });
}

if (Group.find().count() === 0) {
    Group.insert({
        name: "sp-11",
        number_of_person:20
    });

    Group.insert({
        name: "sp-21",
        number_of_person:23
    });

    Group.insert({
        name: "sp-31",
        number_of_person:28
    });

    Group.insert({
        name: "pk-31",
        number_of_person:16
    });
}

if (Subject.find().count() === 0) {
    Subject.insert({
        name: 'Math'
    });

    Subject.insert({
        name: 'OOP'
    });

    Subject.insert({
        name: 'BD'
    });
}

if (Teacher.find().count() === 0) {
    Teacher.insert({
        customer_name: 'Ivanov Ivar Ivanovich',
        age: 25
    });

    Teacher.insert({
        customer_name: 'Severus Snape',
        age: 19
    });

    Teacher.insert({
        customer_name: 'Tom Redl',
        age: 30
    });

    Teacher.insert({
        customer_name: 'Albus Dambelsor',
        age: 30
    });
}


Meteor.startup(function(){
        if (Meteor.users.find().count()==0){
            var userId = Accounts.createUser({email:"admin@admin.com", password:"asdf"});
            Roles.addUsersToRoles(userId, ['admin']);
        }
    }
)


