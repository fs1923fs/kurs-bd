Meteor.users.after.insert (function(userId, doc) {
    var userId = doc._id;
    Roles.addUsersToRoles(userId, ['user']);
} );

Meteor.methods({
    'addAdmin':function(UserId){
        //if (Meteor.user())
        Roles.addUsersToRoles(UserId, ['admin']);
    },
    'findUser':function(Email){
        var user = Meteor.users.findOne({"emails.address":Email});
        if (Meteor.users.find({"emails.address":Email}).count()==0)
            throw new Meteor.Error("Not find user!");
        else
            return user;
    }
})