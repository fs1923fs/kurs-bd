Template.subjectEdit.events({
    'submit form': function(e) {
        e.preventDefault();

        var currentId = this._id;

        var item = {
            name: $(e.target).find('[name=name]').val()
        };

        Subject.update(currentId, {$set: item}, function(error) {
            if (error) {
                // display the error to the user
                alert(error.reason);
            } else {
                Router.go('subjectList', {_id: currentId});
            }
        });
    },

    'click .delete': function(e) {
        e.preventDefault();

        if (confirm("Delete this teacher?")) {
            var currentId = this._id;
            Subject.remove(currentId);
            Router.go('subjectList');
        }
    }
});