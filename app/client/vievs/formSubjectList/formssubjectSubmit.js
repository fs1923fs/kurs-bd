Template.formsSubjectSubmit.events({
    'submit form': function(e) {
        e.preventDefault();

        var item = {
            name: $(e.target).find('[name=name]').val()
        };

        Meteor.call('formsSubjectInsert', item, function(error, result) {
            // ���������� ������ ������������
            if (error)
                $.notify("Name is occupied!", {type: "danger" });
            else {
                $.notify("Form subject added!", {type: "success"});
                while(window.pageYOffset > 0) {
                    window.scrollBy(0, -10);
                }
            }
        });
    }
});