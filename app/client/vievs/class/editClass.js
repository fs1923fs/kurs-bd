Template.editClass.events({
    'submit form': function(e) {
        e.preventDefault();
        var Atribut = {
            day: Number($(e.target).find('[name=day]').val()),
            numberSubject: Number($(e.target).find('[name=numberSubject]').val()),
            teacher: $(e.target).find('[name=teacher]').val(),
            numberGroup: Number($(e.target).find('[name=numberGroup]').val()),
            numberWeek: Number($(e.target).find('[name=numberWeek]').val()),
            subject: $(e.target).find('[name=subject]').val(),
            formsSubject: $(e.target).find('[name=formsSubject]').val(),
            classroom: $(e.target).find('[name=classroom]').val(),
            group:$(e.target).find('[name=group]').val()
        };
        var item = {
            Atributes:Atribut,
            _id:this._id
        };
        Meteor.call('classUpdate', item, function(error, result) {
            // ���������� ������ ������������
            if (error)
                return alert("Error");
        });
        history.back()
    },

    'click .delete': function(e) {
        e.preventDefault();

        if (confirm("Delete this class?")) {
            var currentId = this._id;
            Class.remove(currentId);
            history.back()
        }
    }
});

Template.editClass.helpers({
    subject: function() {
        return Subject.find();
    },
    teacher: function() {
        return Teacher.find();
    },
    formsSubject: function() {
        return FormsSubject.find();
    },
    classroom: function() {
        return Classroom.find();
    },
    group: function() {
        return Group.find();
    }
});