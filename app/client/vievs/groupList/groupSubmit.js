Template.groupSubmit.events({
    'submit form': function(e) {
        e.preventDefault();

        var item = {
            name: $(e.target).find('[name=name]').val(),
            number_of_person: Number($(e.target).find('[name=number_of_person]').val())
        };

        Meteor.call('groupInsert', item, function(error, result) {
            // ���������� ������ ������������
            if (error)
                $.notify("Name is occupied!", {type: "danger" });
            else {
                $.notify("Group added!", {type: "success"});
                while(window.pageYOffset > 0) {
                    window.scrollBy(0, -10);
                }
            };
        });
    }
});