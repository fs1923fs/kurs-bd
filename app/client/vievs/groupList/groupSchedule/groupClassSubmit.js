Template.groupClassSubmit.events({
    'submit form': function(e) {
        e.preventDefault();
        var item = {
            day: Number($(e.target).find('[name=day]').val()),
            numberSubject: Number($(e.target).find('[name=numberSubject]').val()),
            teacher: $(e.target).find('[name=teacher]').val(),
            numberGroup: Number($(e.target).find('[name=numberGroup]').val()),
            numberWeek: Number($(e.target).find('[name=numberWeek]').val()),
            subject: $(e.target).find('[name=subject]').val(),
            formsSubject: $(e.target).find('[name=formsSubject]').val(),
            classroom: $(e.target).find('[name=classroom]').val(),
            group:Group.findOne(Router.current().params._id).name
        };
        Meteor.call('classInsert', item, function(error, result) {
            // ���������� ������ ������������
            if (error)
                $.notify("Nakladka v "+ error.error, {type: "danger" });
            else{
                $.notify("Class added!", {type: "success"});
                while(window.pageYOffset > 0) {
                    window.scrollBy(0, -10);
                }
            }
        });
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    }
});

Template.groupClassSubmit.helpers({
    subject: function() {
        return Subject.find();
    },
    teacher: function() {
        return Teacher.find();
    },
    formsSubject: function() {
        return FormsSubject.find();
    },
    classroom: function() {
        return Classroom.find();
    }
});