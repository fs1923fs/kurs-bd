Template.groupAtributs.helpers({
    subjectT: function(){
        return Subject.findOne(this.subject).name;
    },
    classroomT: function(){
        return Classroom.findOne(this.classroom).name;
    }
});