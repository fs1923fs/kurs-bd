Template.subjectSchedule.helpers({
    subjectName: function(){
        return Class.find({group:Router.current().params._id, day:this.left, numberSubject:this.top});
    },
    subject: function(){
        return Subject.findOne(this.subject).name;
    },
    classroom: function(){
        return Classroom.findOne(this.classroom).name;
    },
    formSubject: function(){
        return FormsSubject.findOne(this.formsSubject).name;
    },
    teacher: function(){
        return Teacher.findOne(this.teacher).customer_name;
    }
});