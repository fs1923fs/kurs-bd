Template.groupSchedule.helpers({
    week: function() {
        var day = 5;
        var numberSubject = 7;
        var weeks = Array();
        for (var i = 1; i <= numberSubject; i++) {
            var row = Array();
            for (var j = 1; j <= day; j++) {
                var item = {
                    top: i,
                    left: j
                };
                row.push(item);
            }
            weeks.push(row);
        }
        return weeks;
    },
    name: function(){
        return Group.findOne(Router.current().params._id).name;
    }
});

Template.groupSchedule.onRendered(function(){
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
});