Template.groupEdit.events({
    'submit form': function(e) {
        e.preventDefault();

        var currentId = this._id;

        var item = {
            name: $(e.target).find('[name=name]').val(),
            number_of_person: Number($(e.target).find('[name=number_of_person]').val())
        };

        Group.update(currentId, {$set: item}, function(error) {
            if (error) {
                // display the error to the user
                alert(error.reason);
            } else {
                Router.go('groupList', {_id: currentId});
            }
        });
    },

    'click .delete': function(e) {
        e.preventDefault();

        if (confirm("Delete this group?")) {
            var currentId = this._id;
            Group.remove(currentId);
            Router.go('groupList');
        }
    }
});