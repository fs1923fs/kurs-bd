Template.classroomSubmit.events({
    'submit form': function(e) {
        e.preventDefault();

        var item = {
            name: $(e.target).find('[name=name]').val(),
            capasity: Number($(e.target).find('[name=capasity]').val())
        };

        Meteor.call('classroomInsert', item, function(error, result) {
            // ���������� ������ ������������
            if (error)
                $.notify("Name is occupied!", {type: "danger" });
            else {
                $.notify("Classrooom added!", {type: "success"});
                while(window.pageYOffset > 0) {
                    window.scrollBy(0, -10);
                }
            }
        });
    }
});