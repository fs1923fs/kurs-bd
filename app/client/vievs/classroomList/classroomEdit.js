Template.classroomEdit.events({
    'submit form': function(e) {
        e.preventDefault();

        var currentId = this._id;

        var item = {
            name: $(e.target).find('[name=name]').val(),
            capasity: Number($(e.target).find('[name=capasity]').val())
        };

        Classroom.update(currentId, {$set: item}, function(error) {
            if (error) {
                // display the error to the user
                alert(error.reason);
            } else {
                Router.go('classroomList', {_id: currentId});
            }
        });
    },

    'click .delete': function(e) {
        e.preventDefault();

        if (confirm("Delete this form subject?")) {
            var currentId = this._id;
            Classroom.remove(currentId);
            Router.go('classroomList');
        }
    }
});