Template.classroomClassSubmit.events({
    'submit form': function(e) {
        e.preventDefault();
        var item = {
            day: Number($(e.target).find('[name=day]').val()),
            numberSubject: Number($(e.target).find('[name=numberSubject]').val()),
            group: $(e.target).find('[name=group]').val(),
            numberGroup: Number($(e.target).find('[name=numberGroup]').val()),
            numberWeek: Number($(e.target).find('[name=numberWeek]').val()),
            subject: $(e.target).find('[name=subject]').val(),
            formsSubject: $(e.target).find('[name=formsSubject]').val(),
            teacher: $(e.target).find('[name=teacher]').val(),
            classroom:Classroom.findOne(Router.current().params._id).name
        };
        Meteor.call('classInsert', item, function(error, result) {
            // ���������� ������ ������������
            if (error)
                $.notify("Nakladka v "+ error.error, {type: "danger" });
            else{
                $.notify("Class added!", {type: "success"});
                while(window.pageYOffset > 0) {
                    window.scrollBy(0, -10);
                }
            }
        });
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    }
});

Template.classroomClassSubmit.helpers({
    subject: function() {
        return Subject.find();
    },
    group: function() {
        return Group.find();
    },
    formsSubject: function() {
        return FormsSubject.find();
    },
    teacher: function() {
        return Teacher.find();
    }
});