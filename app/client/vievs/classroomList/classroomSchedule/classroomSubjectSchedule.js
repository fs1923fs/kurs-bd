Template.classroomSubjectSchedule.helpers({
    subjectName: function(){
        return Class.find({classroom:Router.current().params._id, day:this.left, numberSubject:this.top});
    },
    teacher: function(){
        return Teacher.findOne(this.teacher).customer_name;
    },
    group: function(){
        return Group.findOne(this.group).name;
    },
    subject: function(){
        return Subject.findOne(this.subject).name;
    },
    formSubject: function(){
        return FormsSubject.findOne(this.formSubject).name;
    }
});
