Template.addAdmin.events({
    'submit form': function(e) {
        e.preventDefault();

        Meteor.call('findUser', email, function(error,params) {
            if (error){
                $.notify(error.error, {type: "danger" });
            }else {
                if (confirm("Delete this group?")) {
                    Meteor.call('addAdmin', params);
                    $.notify("vce ok", {type: "success" });
                }
            }
        });

    }
});