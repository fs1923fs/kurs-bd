Template.teacherEdit.events({
    'submit form': function(e) {
        e.preventDefault();

        var currentId = this._id;

        var item = {
            customer_name: $(e.target).find('[name=customer_name]').val(),
            age: Number($(e.target).find('[name=age]').val())
        };

        Teacher.update(currentId, {$set: item}, function(error) {
            if (error) {
                // display the error to the user
                alert(error.reason);
            } else {
                Router.go('teacherList', {_id: currentId});
            }
        });
    },

    'click .delete': function(e) {
        e.preventDefault();

        if (confirm("Delete this teacher?")) {
            var currentId = this._id;
            Teacher.remove(currentId);
            Router.go('teacherList');
        }
    }
});