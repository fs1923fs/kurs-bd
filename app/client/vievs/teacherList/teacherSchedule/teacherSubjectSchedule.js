Template.teacherSubjectSchedule.helpers({
    subjectName: function(){
        return Class.find({teacher:Router.current().params._id, day:this.left, numberSubject:this.top});
    },
    subject: function(){
        if (Subject.find(this.subject).count()>0)
            return Subject.findOne(this.subject).name;
    },
    classroom: function(){
        if (Classroom.find(this.classroom).count()>0)
            return Classroom.findOne(this.classroom).name;
    },
    formSubject: function(){
        return FormsSubject.findOne(this.formsSubject).name;
    },
    group: function(){
        return Group.findOne(this.group).name;
    }
});