Template.teacherSubmit.events({
    'submit form': function(e) {
        e.preventDefault();

        var item = {
            customer_name: $(e.target).find('[name=customer_name]').val(),
            age: Number($(e.target).find('[name=age]').val())
        };

        Meteor.call('teacherInsert', item, function(error, result) {
            // ���������� ������ ������������
            if (error)
                $.notify("Name is occupied!", {type: "danger" });
            else {
                $.notify("Teacher added!", {type: "success"});
                while(window.pageYOffset > 0) {
                    window.scrollBy(0, -10);
                }
            }
        });
    }
});