Group = new Mongo.Collection('group');

Group.allow({
    remove: function(userId, doc) {
        // ��������� ������� ������ ���� ������������ ���������
        return !! userId;
    },
    update:function(userId, doc) {
        // ��������� ������� ������ ���� ������������ ���������
        return !! userId;
    }
});

Meteor.methods({
    groupInsert: function(Attributes) {
        //check(postAttributes, Schemas.Group);
        check(Attributes, {name: String, number_of_person:Number});

        if (Group.find({name:Attributes.name}).count()>0)
            throw new Meteor.error("User already exists");

        var postId = Group.insert(Attributes);
        return {
            _id: postId
        };
    }
});
