Classroom = new Mongo.Collection('classroom');

Classroom.allow({
    remove: function(userId, doc) {
        // ��������� ������� ������ ���� ������������ ���������
        return !! userId;
    },
    update:function(userId, doc) {
        // ��������� ������� ������ ���� ������������ ���������
        return !! userId;
    }
});

Meteor.methods({
    classroomInsert: function(Attributes) {
        check(Attributes, {name: String, capasity:Number});

        if (Classroom.find({name:Attributes.name}).count()>0)
            throw new Meteor.error("User already exists");

        var postId = Classroom.insert(Attributes);
        return {
            _id: postId
        };
    }
});