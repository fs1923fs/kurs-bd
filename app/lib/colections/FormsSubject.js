FormsSubject = new Mongo.Collection('formsSubject');

FormsSubject.allow({
    remove: function(userId, doc) {
        // ��������� ������� ������ ���� ������������ ���������
        return !! userId;
    },
    update:function(userId, doc) {
        // ��������� ������� ������ ���� ������������ ���������
        return !! userId;
    }
});

Meteor.methods({
    formsSubjectInsert: function(Attributes) {
        check(Attributes, {name: String});

        if (FormsSubject.find({name:Attributes.name}).count()>0)
            throw new Meteor.error("User already exists");

        var postId =FormsSubject.insert(Attributes);
        return {
            _id: postId
        };
    }
});