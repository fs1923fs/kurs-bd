Teacher = new Mongo.Collection('teacher');

Teacher.allow({
    remove: function(userId, doc) {
        // ��������� ������� ������ ���� ������������ ���������
        return !! userId;
    },
    update:function(userId, doc) {
        // ��������� ������� ������ ���� ������������ ���������
        return !! userId;
    }
});

Meteor.methods({
    teacherInsert: function(Attributes) {
        //check(postAttributes, Schemas.Group);
        if (Teacher.find({customer_name:Attributes.customer_name}).count()>0)
            throw new Meteor.error("User already exists");
        check(Attributes, {customer_name: String, age:Number});
        var postId = Teacher.insert(Attributes);
        return {
            _id: postId
        };
    }
});
