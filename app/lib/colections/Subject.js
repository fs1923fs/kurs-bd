Subject = new Mongo.Collection('subject');

Subject.allow({
    remove: function(userId, doc) {
        // ��������� ������� ������ ���� ������������ ���������
        return !! userId;
    },
    update:function(userId, doc) {
        // ��������� ������� ������ ���� ������������ ���������
        return !! userId;
    }
});

Meteor.methods({
    subjectInsert: function(Attributes) {
        check(Attributes, {name: String});

        if (Subject.find({name:Attributes.name}).count()>0)
            throw new Meteor.error("User already exists");

        var postId =Subject.insert(Attributes);
        return {
            _id: postId
        };
    }
});