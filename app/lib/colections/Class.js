Class = new Mongo.Collection('class');

Class.allow({
    remove: function(userId, doc) {
        // ��������� ������� ������ ���� ������������ ���������
        return !! userId;
    },
    update:function(userId, doc) {
        // ��������� ������� ������ ���� ������������ ���������
        return !! userId;
    }
});

Meteor.methods({
    classInsert: function(Attributes) {
        check(Attributes, {
            day: Number,
            numberSubject: Number,
            numberGroup: Number,
            numberWeek: Number,
            teacher: String,
            subject: String,
            formsSubject: String,
            classroom: String,
            group: String
        });

        if (Classroom.find({name:Attributes.classroom}).count()===0)
            throw new Meteor.error("Classroom");

        if (Teacher.find({customer_name:Attributes.teacher}).count()===0)
            throw new Meteor.error("Teacher");

        if (FormsSubject.find({name:Attributes.formsSubject}).count()===0)
            throw new Meteor.error("User already exists");

        if (Group.find({name:Attributes.group}).count()===0)
            throw new Meteor.error("Group");

        if (Subject.find({name:Attributes.subject}).count()==0)
            throw new Meteor.error("Subject");

        Attributes.teacher=Teacher.findOne({customer_name:Attributes.teacher})._id;
        Attributes.subject=Subject.findOne({name: Attributes.subject})._id;
        Attributes.formsSubject=FormsSubject.findOne({name:Attributes.formsSubject})._id;
        Attributes.classroom=Classroom.findOne({name:Attributes.classroom})._id;
        Attributes.group=Group.findOne({name:Attributes.group})._id;

        //�������� �������� �����
        {if (Attributes.numberGroup==0 &&
            Attributes.numberWeek==0 &&
            Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, group:Attributes.group}).count()>0)
            throw new Meteor.Error(Group.findOne(Attributes.group).name);

        if (Attributes.numberGroup==1 &&
            Attributes.numberWeek==0 &&
            (Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberGroup:1, group:Attributes.group}).count() +
            Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberGroup:0, group:Attributes.group}).count())>0)
            throw new Meteor.Error(Group.findOne(Attributes.group).name);

        if (Attributes.numberGroup==2 &&
            Attributes.numberWeek==0 &&
            (Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberGroup:2, group:Attributes.group}).count() +
            Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberGroup:0, group:Attributes.group}).count())>0)
            throw new Meteor.Error(Group.findOne(Attributes.group).name);

        if (Attributes.numberGroup==0 &&
            Attributes.numberWeek==1 &&
            (Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberWeek:1, group:Attributes.group}).count() +
            Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberWeek:0, group:Attributes.group}).count())>0)
            throw new Meteor.Error(Group.findOne(Attributes.group).name);

        if (Attributes.numberGroup==0 &&
            Attributes.numberWeek==2 &&
            (Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberWeek:2, group:Attributes.group}).count() +
            Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberWeek:0, group:Attributes.group}).count())>0)
            throw new Meteor.Error(Group.findOne(Attributes.group).name);

        if ((Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberWeek:Attributes.numberWeek,
                numberGroup:Attributes.numberGroup, group:Attributes.group}).count() +
            Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberWeek:0,
                numberGroup:Attributes.numberGroup, group:Attributes.group}).count()+
            Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberWeek:0,
                numberGroup:0, group:Attributes.group}).count())>0)
            throw new Meteor.Error(Group.findOne(Attributes.group).name);};

        //������� �������� �������
        {
            if (Attributes.numberGroup==0 &&
            Attributes.numberWeek==0 &&
            Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, teacher:Attributes.teacher}).count()>0)
            throw new Meteor.Error(Teacher.findOne(Attributes.teacher).customer_name);

            if (Attributes.numberGroup==1 &&
                Attributes.numberWeek==0 &&
                (Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberGroup:1,
                    teacher:Attributes.teacher}).count() +
                Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberGroup:0,
                    teacher:Attributes.teacher}).count())>0)
                throw new Meteor.Error(Teacher.findOne(Attributes.teacher).customer_name);

            if (Attributes.numberGroup==2 &&
                Attributes.numberWeek==0 &&
                (Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberGroup:2,
                    teacher:Attributes.teacher}).count() +
                Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberGroup:0,
                    teacher:Attributes.teacher}).count())>0)
                throw new Meteor.Error(Teacher.findOne(Attributes.teacher).customer_name);

            if (Attributes.numberGroup==0 &&
                Attributes.numberWeek==1 &&
                (Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberWeek:1,
                    teacher:Attributes.teacher}).count() +
                Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberWeek:0,
                    teacher:Attributes.teacher}).count())>0)
                throw new Meteor.Error(Teacher.findOne(Attributes.teacher).customer_name);

            if (Attributes.numberGroup==0 &&
                Attributes.numberWeek==2 &&
                (Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberWeek:2,
                    teacher:Attributes.teacher}).count() +
                Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberWeek:0,
                    teacher:Attributes.teacher}).count())>0)
                throw new Meteor.Error(Teacher.findOne(Attributes.teacher).customer_name);

            if ((Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberWeek:Attributes.numberWeek,
                    numberGroup:Attributes.numberGroup, teacher:Attributes.teacher}).count() +
                Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberWeek:0,
                    numberGroup:Attributes.numberGroup, teacher:Attributes.teacher}).count()+
                Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, numberWeek:0,
                    numberGroup:0, teacher:Attributes.teacher}).count())>0)
                throw new Meteor.Error(Teacher.findOne(Attributes.teacher).customer_name);
        };

        //�������� �������� �����
        {
            if ((Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, classroom:Attributes.classroom,
                    numberWeek:0}).count()>0) ||
                (Class.find({day:Attributes.day, numberSubject:Attributes.numberSubject, classroom:Attributes.classroom,
                    numberWeek:Attributes.numberWeek}).count()>0))
                throw new Meteor.Error(Classroom.findOne(Attributes.classroom).name);
        };

        //�������� ��������� �������
        if (Classroom.findOne(Attributes.classroom).capasity-Group.findOne(Attributes.group).number_of_person<0)
            throw new Meteor.Error("Vmist!");

        var postId = Class.insert(Attributes);
        return {
            _id: postId
        };
    },
    classUpdate: function(item) {
        _id=item._id;
        Attributes=item.Atributes;
        check(Attributes, {
            day: Number,
            numberSubject: Number,
            numberGroup: Number,
            numberWeek: Number,
            teacher: String,
            subject: String,
            formsSubject: String,
            classroom: String,
            group: String
        });

        if (Classroom.find({name:Attributes.classroom}).count()===0)
            throw new Meteor.error("Classroom");

        if (Teacher.find({customer_name:Attributes.teacher}).count()===0)
            throw new Meteor.error("Teacher");

        if (FormsSubject.find({name:Attributes.formsSubject}).count()===0)
            throw new Meteor.error("User already exists");

        if (Group.find({name:Attributes.group}).count()===0)
            throw new Meteor.error("Group");

        if (Subject.find({name:Attributes.subject}).count()==0)
            throw new Meteor.error("Subject");

        Attributes.teacher=Teacher.findOne({customer_name:Attributes.teacher})._id;
        Attributes.subject=Subject.findOne({name: Attributes.subject})._id;
        Attributes.formsSubject=FormsSubject.findOne({name:Attributes.formsSubject})._id;
        Attributes.classroom=Classroom.findOne({name:Attributes.classroom})._id;
        Attributes.group=Group.findOne({name:Attributes.group})._id;


        var postId = Class.update(_id,{$set: Attributes});;
        return {
            _id: postId
        };
    }
});