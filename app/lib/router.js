Router.configure({
    layoutTemplate: 'layout'

});

Router.route('groupList', {path: '/'});

Router.route('groupSchedule', {
    path: '/group/:_id'
});

Router.route('teacherSchedule', {
    path: '/teacher/:_id'
});

Router.route('classroomSchedule', {
    path: '/classroom/:_id'
});

Router.route('groupEdit', {
    path: '/group/:_id/edit',
    data: function() { return Group.findOne(this.params._id); }
});

Router.route('teacherEdit', {
    path: '/teacher/:_id/edit',
    data: function() { return Teacher.findOne(this.params._id); }
});

Router.route('subjectEdit', {
    path: '/subject/:_id/edit',
    data: function() { return Subject.findOne(this.params._id); }
});

Router.route('editClass', {
    path: '/class/:_id/edit',
    data: function() { return Class.findOne(this.params._id); }
});

Router.route('formsSubjectEdit', {
    path: '/formsSubject/:_id/edit',
    data: function() { return FormsSubject.findOne(this.params._id); }
});

Router.route('classroomEdit', {
    path: '/classroom/:_id/edit',
    data: function() { return Classroom.findOne(this.params._id); }
});

Router.route('teacherList', {path: '/teacher'});

Router.route('formsSubjectList', {path: '/formsSubject'});

Router.route('classroomList', {path: '/classroom'});

Router.route('subjectList', {path: '/subject'});

Router.route('addAdmin', {path: '/addAdmin'});

Router.onBeforeAction('loading');